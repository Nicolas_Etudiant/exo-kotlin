package compterMots

import org.junit.Test
import kotlin.test.assertEquals

class CompterMotsTest() {
    @Test fun CompteVide() {
        assertEquals(compterMots(""),0)
    }
    @Test fun CompteUnMot() {
        assertEquals(compterMots("test"),1)
    }
    @Test fun CompteUnMotBlancAvAp() {
        assertEquals(compterMots("   test   "),1)
    }
    @Test fun Compte4MotsBlancAvApEtTab() {
        assertEquals(compterMots("   ceci est un \ttest   "),4)
    }
    @Test fun Compte4MotsPlusRetourChariot() {
        assertEquals(compterMots("Ceci est\nun test. \n"),4)
    }
}